﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParrarelBackground : MonoBehaviour {

	[System.Serializable]
	public struct ParrarelLayer
	{
		public SpriteRenderer sprite;
		public byte layer;
	}

	/// <summary>
	/// Component for manipulating every images.
	/// </summary>
	public class ParralelComponent : MonoBehaviour
	{
		private int layerImage;
		private float parrarelSpeedMultiplier = 1.0f;

		private void Start()
		{
			Debug.Log("Success Init Parrarel of " + gameObject.name);
		}

		/// <summary>
		/// Settings up the parraler parameter.
		/// </summary>
		/// <param name="layerImage"></param>
		/// <param name="parrarelSpeed"></param>
		public void SetParameter(int layerImage, float parrarelSpeedMultiplier = 1.0f)
		{
			this.layerImage = layerImage;
			this.parrarelSpeedMultiplier = parrarelSpeedMultiplier;
		}

		// Execute every frames.
		private void Update()
		{
			transform.SetPositionAndRotation
				(transform.position + Vector3.left * ((layerImage * 0.01f) * parrarelSpeedMultiplier), Quaternion.identity);
		}
	}

	[SerializeField] private ParrarelLayer[] m_ImageParralel;
	private Transform[] imagesTransform;

	// Use this for initialization
	void Start () {

		// Init Images Parralel.
		InitiailzingImage();

	}
	
	void InitiailzingImage()
	{
		List<Transform> imageTransformList = new List<Transform>();
		foreach (ParrarelLayer item in m_ImageParralel)
		{
			imageTransformList.Add(item.sprite.transform);
		}
		imagesTransform = imageTransformList.ToArray();

		ParralelComponent component;

		// Adding an Component for each parrarel images.
		for (int index = 0; index < imagesTransform.Length; index++)
		{
			component = imagesTransform[index].gameObject.AddComponent<ParralelComponent>();
			component.SetParameter(m_ImageParralel[index].layer);
		}
	}

}