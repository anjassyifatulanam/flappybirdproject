﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDebugable
{
	bool debug { get; set; }
}

[RequireComponent(typeof(AudioSource))]
public class GameManager : MonoBehaviour, IDebugable {

	public static GameManager MainInstance
	{
		get
		{
			return FindObjectOfType<GameManager>();
		}
	}

	// Nilai score akan di stor di variable ini.
	private int m_score;

	// NIlai tertinggi yang di dapat.
	private int m_highScores;

	public int Score
	{
		get
		{
			return m_score;
		}
		set
		{
			m_score = value;
			m_scoreValue.text = "<color=yellow>" + m_score.ToString() + "</color>";
		}
	}
	public int HighScore { get; set; }

	public bool debug { get; set; }
	public bool isBegin { get; set; }

	// Bird Manager.
	public GameObject birdCharacter { get; set; }
	private Vector2 birdStartPosition;

	[Header("Music")]
	private AudioSource m_AudioMusic;
	[SerializeField] private AudioClip m_musicBgm;

	[Header("Field Manager")]
	[SerializeField] private BoxCollider2D upperCollider;
	[SerializeField] private BoxCollider2D downCollider;

	[Header("Canvas Manager")]
	[SerializeField] private TMPro.TextMeshProUGUI m_scoreValue;

	[SerializeField] private GameObject gameOverPanel;
	[SerializeField] private GameObject startingPanel;

	[Header("Obstacle Spawner")]
	[SerializeField] private GameObject m_obstacleSpawnUp;
	[SerializeField] private GameObject m_obstacleSpawnDown;

	[SerializeField] private GameObject[] upperObstacle;
	[SerializeField] private GameObject[] downObstacle;

	[Header("Game Over Manager")]
	[SerializeField] private GameOverManager gameOverManager;

	public BoxCollider2D getUpperCollider
	{
		get { return upperCollider; }
	}
	public BoxCollider2D getDownCollider
	{
		get { return downCollider; }
	}
	
	// Initialzing.
	private void Start()
	{
		// Get Audio Source file.
		m_AudioMusic = GetComponent<AudioSource>();

		InitCanvas();
		StartGame();

		// Set first position.
		birdStartPosition = birdCharacter.transform.position;

	}
	private void Update()
	{
		if (!isBegin)
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				isBegin = true;
				startingPanel.SetActive(false);
				Time.timeScale = 1;
			}
		}
	}
	void InitCanvas()
	{

		// Initialzing active game object.
		gameOverPanel.SetActive(false);
		startingPanel.SetActive(false);


	}

	public void ResetGame()
	{
		GameOverManager.isGameOver = false;
		MainInstance.gameOverPanel.SetActive(false);

		// Resume
		Time.timeScale = 1;

		// Reset current bird position.
		birdCharacter.transform.SetPositionAndRotation(birdStartPosition, Quaternion.identity);
		birdCharacter.GetComponent<BirdController>().birdRigidBody.velocity = Vector3.zero;

		GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Obstacle");
		foreach (GameObject item in obstacles)
		{
			Destroy(item);
		}

		Score = 0;
	}
	public void StartGame()
	{

		isBegin = false;
		startingPanel.SetActive(true);

		Time.timeScale = 0;
		
		// Spawn managers.
		CancelInvoke();
		InvokeRepeating("SpawnObstacle", 0.5f, 5f);

	}
	public void GameOver()
	{

		GameOverManager.isGameOver = true;

		// Pause all time.
		Time.timeScale = 0;

		// High Scores checker
		if (Score > HighScore)
			HighScore = Score;

		gameOverManager.Score = Score;
		gameOverManager.HighScore = HighScore;

		// Init panel system.
		if (!gameOverPanel.activeInHierarchy)
			gameOverPanel.SetActive(true);

		gameOverManager.animator.SetTrigger("Show");
	}

	void SpawnObstacle()
	{

		int randomUp = Random.Range(-1, upperObstacle.Length);
		int randowDown = Random.Range(-1, downObstacle.Length);

		if (randomUp >= 0)
			Instantiate(upperObstacle[randomUp], m_obstacleSpawnUp.transform.position, Quaternion.identity);

		if (randowDown >= 0)
			Instantiate(downObstacle[randowDown], m_obstacleSpawnDown.transform.position, Quaternion.identity);

	}
}
