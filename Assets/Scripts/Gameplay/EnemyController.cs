﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

	[Header("Movement Settings")]
	[SerializeField] private float movementSpeed;
	[SerializeField] private Vector2 movementDirection;

	[Space]
	[SerializeField] private float delayDestory;

	// Use this for initialization
	void Start () {

		// Start Routines.
		StartCoroutine(EnemyYieldDestroy());

	}
	
	IEnumerator EnemyYieldDestroy()
	{
		yield return new WaitForSeconds(delayDestory);
		Destroy(gameObject);
	}

	// Update is called once per frame
	void Update () {

		if(Time.timeScale != 0){

			// Movement Enemy
			transform.SetPositionAndRotation((Vector2)transform.position + (movementDirection * (movementSpeed * 0.1f)), Quaternion.identity);

		}
	}
}
