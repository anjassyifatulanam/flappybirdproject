﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BirdController : MonoBehaviour, IDebugable {

	[SerializeField] private KeyCode flyInputKey;
	private Rigidbody2D m_characterRigidBody;
	[SerializeField] private float flyForce;

	public Rigidbody2D birdRigidBody
	{
		get { return m_characterRigidBody; }
	}

	private float YVelocity;
	private float tiltZAngle;

	[SerializeField] private bool debugging;
	public bool debug { get; set; }

	// Use this for initialization
	void Start () {

		// Debugging.
		debug = debugging;

		// Getting an RigidBody Component.
		m_characterRigidBody = GetComponent<Rigidbody2D>();

		// Assign to manager.
		GameManager.MainInstance.birdCharacter = gameObject;

	}
	
	// Update is called once per frame
	void Update () {

		// Tilt effect.
		TiltAngle();

		// Gameplay Mechanic.
		if (Input.GetKeyDown(flyInputKey))
		{
			m_characterRigidBody.velocity = Vector3.zero;
			m_characterRigidBody.AddForce(Vector3.up * flyForce);
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{

		if (collision.gameObject.name == GameManager.MainInstance.getUpperCollider.gameObject.name
			|| collision.gameObject.name == GameManager.MainInstance.getDownCollider.gameObject.name
			|| collision.gameObject.tag == "Obstacle")
		{
			if(debug)
				Debug.Log("Game Over");

			GameManager.MainInstance.GameOver();
		}

		if (collision.tag == "Scoring")
			GameManager.MainInstance.Score += 200;


	}
	
	void TiltAngle()
	{

		// Velocity Y of the Rigidbody.
		YVelocity = m_characterRigidBody.velocity.y;

		Vector3 rotationDir = Vector3.zero;
		Quaternion quaternionRotation = transform.rotation;

		rotationDir.z = (YVelocity / 0.5f);
		quaternionRotation.eulerAngles = rotationDir;

		// Rotation Tilt.
		transform.rotation = quaternionRotation;

	}
}
