﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameOverManager : MonoBehaviour {

	[SerializeField] TextMeshProUGUI m_highScoreText;
	[SerializeField] TextMeshProUGUI m_scoreText;

	[SerializeField] Animator m_animator;
	public Animator animator { get { return m_animator; } }
	public static bool isGameOver;

	private void Awake()
	{
		// Get Animator component.
		m_animator = GetComponent<Animator>();

	}

	public int HighScore
	{
		set
		{
			m_highScoreText.text = "<color=yellow>High Score</color>" + " : " + value.ToString();
		}
	}
	public int Score
	{
		set
		{
			m_scoreText.text = "<color=yellow>Scores</color>" + " : " + value.ToString();
		}
	}

	private void Update()
	{
		if (isGameOver)
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				GameManager.MainInstance.ResetGame();
			}
		}

	}
}
